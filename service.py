# control Kodi using a rotary encoder
# Copyright (C) 2023 Jakob Meier <comcloudway@ccw.icu>

import sys
import time

use_xbmc = True
try:
    import xbmc
except:
    use_xbmc = False
from socket import *

try:
    from kodi.xbmcclient import *
except:
    sys.path.append('/usr/share/pyshared/xbmc')
    from xbmcclient import *

# returns 1 if the given gpio pin is high
def read_gpio(num):
    file = open('/sys/class/gpio/gpio'+str(num)+'/value')
    cont = file.readline()
    file.close();
    if cont.strip() == '1':
        return 1
    else:
        return 0

# exports the pin using the sysfs interface
def export(num):
    file = open('/sys/class/gpio/export', 'w')
    file.write(str(num))
    file.close()

# unexports the pin using the sysfs interface
def unexport(num):
    file = open('/sys/class/gpio/unexport', 'w')
    file.write(str(num))
    file.close()

PIN_CLK=5
PIN_DT=6
PIN_SWT=3

LONG_PRESS_DURATION=1 # duration in seconds

EV_UP = "VolumeUp"
EV_DOWN = "VolumeDown"
EV_BTN_SHORT = "PlayPause"
EV_BTN_LONG = "PreviousMenu"

if __name__ == '__main__':
    monitor = None
    if use_xbmc:
        monitor = xbmc.Monitor()

    # export gpio
    export(PIN_CLK) # Up (CLK)
    export(PIN_DT) # Down (DT)
    export(PIN_SWT) # BTN (SWT)

    last_up = 2
    last_down = 2
    last_btn = 2
    timer = time.time()

    pressed_up = 0
    pressed_down = 0

    cdown = 0
    cup = 0

    ip = "localhost"
    port = 9777
    addr = (ip, port)
    sock = socket(AF_INET,SOCK_DGRAM)

    while not use_xbmc or not monitor.abortRequested():
        # do stuff
        up = read_gpio(PIN_CLK)
        down = read_gpio(PIN_DT)
        btn = read_gpio(PIN_SWT)

        task = ""
        btn_task = ""

        if up != last_up and last_up != 2:
            if last_down != up:
                # vol UP
                task = "up"
                pressed_up+=1
            else:
                # vol DOWN
                task = "down"
                pressed_down+=1

        if btn != last_btn and last_btn != 2:
            if btn == 0:
                timer = time.time()
            else:
                diff = time.time() - timer
                if diff < LONG_PRESS_DURATION:
                    # short press
                    btn_task = "short"
                else:
                    # long press
                    btn_task = "long"

        last_up = up
        last_down = down
        last_btn = btn

        msg = ""

        # Process rotary action
        if task == "up":
            if pressed_up % 2 == 0:
                cup+=1
                if cup > 1:
                    cup = 0
                    # send vol up
                    print("Volume Up")
                    msg = EV_UP
        elif task == "down":
            if pressed_down % 2 == 0:
                cdown+=1
                if cdown > 1:
                    cdown = 0
                    # send vol down
                    print("Volume Down")
                    msg = EV_DOWN

        if msg != "":
            packet = PacketACTION(actionmessage=msg, actiontype=ACTION_BUTTON)
            packet.send(sock, addr, uid=0)
            msg = ""

        # Process button action
        if btn_task == "short":
            # send short press
            print("Short press")
            msg = EV_BTN_SHORT
        elif btn_task == "long":
            # send long press
            print("Long press")
            msg = EV_BTN_LONG

        if msg != "":
            packet = PacketACTION(actionmessage=msg, actiontype=ACTION_BUTTON)
            packet.send(sock, addr, uid=0)
            msg = ""

    # End of loop
    unexport(PIN_CL)
    unexport(PIN_DT)
    unexport(PIN_SWT)
